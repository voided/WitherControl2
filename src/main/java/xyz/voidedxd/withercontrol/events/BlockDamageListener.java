package xyz.voidedxd.withercontrol.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import xyz.voidedxd.withercontrol.WitherControl;

import java.util.Iterator;
import java.util.function.Predicate;

public class BlockDamageListener implements Listener {
    private WitherControl plugin;

    public BlockDamageListener(WitherControl plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void WitherBlockBreak(EntityChangeBlockEvent event) {
        Block block = event.getBlock();
        Entity entity = event.getEntity();

        if(entity.getType() == EntityType.WITHER) {
            if(!plugin.isBreakable(block.getType(), "wither")) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void CreeperExplode(EntityExplodeEvent event) {
        event.blockList().removeIf(notCreeperBreakable());
    }

    public Predicate<Block> notCreeperBreakable() {
        return block -> !plugin.isBreakable(block.getType(), "creeper");
    }
}
