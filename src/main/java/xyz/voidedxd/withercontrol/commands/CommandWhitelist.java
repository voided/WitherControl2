package xyz.voidedxd.withercontrol.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import xyz.voidedxd.withercontrol.WitherControl;

public class CommandWhitelist implements CommandExecutor {
    private WitherControl plugin;

    public CommandWhitelist(WitherControl plugin) {
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Not enough arguments!");
            return true;
        }

        if(!(args[0].equals("add") || args[0].equals("remove"))) {
            sender.sendMessage(ChatColor.RED + "First argument should be 'add' or 'remove'.");
            return true;
        }

        if(args[0].equals("add")) {
            if (args.length < 3 || !(args[2].equals("wither") || args[2].equals("creeper") || args[2].equals("both"))) {
                sender.sendMessage(ChatColor.RED + "Third argument should be 'wither', 'creeper', or 'both'.");
                return true;
            }
        }

        if(!sender.hasPermission("withercontrol.whitelist.modify")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to modify the WitherControl whitelist.");
            return true;
        }

        // TODO: open ended crap

        Material mat = Material.getMaterial(args[1]);

        if(mat != null) {
           if(mat.isBlock()) {
               if(args[0].equals("add")) {
                   plugin.getConfig().set(mat.toString(), args[2].toLowerCase());
                   sender.sendMessage(ChatColor.GREEN + "Added that block to the " + args[2].toLowerCase() + " whitelist.");
               } else if(args[0].equals("remove")) {
                   plugin.getConfig().set(mat.toString(), null);
                   sender.sendMessage(ChatColor.GREEN + "Removed that block from the whitelist.");
               }
               plugin.saveConfig();
           } else {
               sender.sendMessage(ChatColor.RED + "That material is not a block!");
           }
        } else {
            sender.sendMessage(ChatColor.RED + "That material does not exist!");
        }
        return true;
    }
}
