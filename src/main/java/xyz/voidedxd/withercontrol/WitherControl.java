package xyz.voidedxd.withercontrol;

import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.voidedxd.withercontrol.commands.CommandWhitelist;
import xyz.voidedxd.withercontrol.events.BlockDamageListener;

import java.util.logging.Logger;

public final class WitherControl extends JavaPlugin {

    private Logger LOGGER;

    @Override
    public void onEnable() {
        LOGGER = getLogger();
        LOGGER.info("Enabling WitherControl2! Hello, Minecraft!");

        this.getServer().getPluginManager().registerEvents(new BlockDamageListener(this), this);
        this.getCommand("wcwl").setExecutor(new CommandWhitelist(this));

        loadConfig();
    }

    public void loadConfig() {
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }

    @Override
    public void onDisable() {
        LOGGER.info("Disabling WitherControl2!");
        this.saveConfig();
    }

    public boolean isBreakable(Material material, String mob) {
        Object nullableResult = this.getConfig().get(material.toString());
        String result = "";

        if(nullableResult != null) {
            result = nullableResult.toString();
        }

        return result.equals(mob) || result.equals("both");
    }
}
